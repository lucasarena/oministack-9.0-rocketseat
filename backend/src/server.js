const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes');
const cors = require('cors');
const path = require('path');
const socketio = require('socket.io');
const http = require('http');

const app = express();
const server = http.Server(app);
const io = socketio(server);

const connectedUsers = {};

mongoose.connect('mongodb+srv://oministack:ben101112@rocketcluster-u3tos.gcp.mongodb.net/semana09?retryWrites=true&w=majority',{
    useNewUrlParser : true,
    useUnifiedTopology : true
});

io.on('connection', socket => {
    const { user_id } = socket.handshake.query;

    connectedUsers[user_id] = socket.id;
});

app.use((req,res,next) => {
    req.io = io;
    req.connectedUsers = connectedUsers;

    return next();
});

// req.query = Acessar as querys params passado via get para filtros Ex: localhost:3333/user?filtro=Lucas.
// req.params = Acessar os parametros passado na rota Ex: localhost:3333/user/1.
// req.body = Acessar corpo da requisição (para criação, edição e etc).

app.use(cors());
app.use(express.json());
app.use(routes); 
app.use('/files', express.static(path.resolve(__dirname,'..','uploads')));
server.listen(3333);