import React, { useState, useMemo } from 'react';
import api from '../../services/api';

import camera from '../../assets/camera.svg';
import './style.css';

export default function New({ history }) {
    const [thumbnail, setThumbnail] = useState(null);
    const [company, setCompany] = useState('');
    const [techs, setTechs] = useState('');
    const [price, setPrice] = useState('');

    const preview = useMemo(() => {
            return thumbnail ? URL.createObjectURL(thumbnail) : null
        }, [thumbnail]
    );

    async function handleSubmit(event) {
        event.preventDefault();

        const user_id = localStorage.getItem('user');
        const data = new FormData();

        data.append('thumbnail',thumbnail);
        data.append('company',company);
        data.append('price',price);
        data.append('techs',techs);

        await api.post('/spot', data, {
            headers : { user_id } 
        });

        history.push('/dashboard');
    }

    return(
        <React-Fragment>
            <form onSubmit={handleSubmit}>
                <label 
                    id="thumbnail" 
                    className={thumbnail ? 'has-thumbnail' : ''}
                    style={{ backgroundImage: `url(${preview})`}}
                >
                    <input type="file" onChange={event => setThumbnail(event.target.files[0])} />
                    <img src={camera} alt="Select img"/>
                </label>

                <label htmlFor="company">Empresa *</label>
                <input 
                    type="text" 
                    id="company"
                    placeholder="Sua empresa incrível"
                    value={company}
                    onChange={event => setCompany(event.target.value)}
                />

                <label htmlFor="techs">Tecnólogias * <span>(Separadas por vírgula)</span></label>
                <input 
                    type="text" 
                    id="techs"
                    placeholder="Quais tecnologias usam"
                    value={techs}
                    onChange={event => setTechs(event.target.value)}
                />

                <label htmlFor="price">Valor da diária * <span>(Em branco para gratuita)</span></label>
                <input 
                    type="text" 
                    id="price "
                    placeholder="Valor cobrado por dia"
                    value={price}
                    onChange={event => setPrice(event.target.value)}
                />

                <button className="btn">Cadastrar</button>
            </form>
        </React-Fragment>    
    )
}