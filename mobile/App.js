import React from 'react';
import { YellowBox } from 'react-native';
import Routes from './src/routes';

import { StyleSheet, Text, View } from 'react-native';

YellowBox.ignoreWarnings([
  'Unrecognized WebSocket'
]);

export default function App() {
  return <Routes />
}