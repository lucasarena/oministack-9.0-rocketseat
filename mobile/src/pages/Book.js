import React, { useState } from 'react';
import { Text, Alert, TouchableOpacity, AsyncStorage, TextInput ,StyleSheet, SafeAreaView } from 'react-native';
import api from '../services/api';

export default function Book({ navigation }) {
    const id = navigation.getParam('id');
    const [date,setDate] = useState('');

    async function handleSubmit() {
        const user_id = await AsyncStorage.getItem('user');

        await api.post(`/spot/${id}/bookings`, {
            date
        }, { 
            headers: { user_id }
        });

        Alert.alert('Solicitação de reserva enviada');

        navigation.navigate('List');
    }

    function handleCancel() {
        navigation.navigate('List');
    }

    return (
    <SafeAreaView style={styles.container}>
        <Text style={styles.label}>Data de interesse *</Text>
        <TextInput 
            style={styles.input}
            placeholderTextColor="#999"
            autoCorrect={false}
            autoCapitalize="words"
            placeholder='Qual a data você quer reservar?'
            value={date}
            onChangeText={setDate}
        />
        <TouchableOpacity onPress={handleSubmit} style={styles.button}>
            <Text style={styles.buttonText}>Solicitar Reserva</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={handleCancel} style={[styles.button,styles.buttonCancel]}>
            <Text style={styles.buttonText}>Cancelar</Text>
        </TouchableOpacity>
    </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        margin: 30,
    },

    label: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8,
        marginTop: 30
    },

    input: {
        borderWidth: 1,
        borderColor: '#ddd',
        paddingHorizontal: 20,
        fontSize: 16,
        color: '#444',
        height: 44,
        marginBottom: 20,
        borderRadius: 2
    },
    button: {
        height: 42,
        backgroundColor: '#f05a5b',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16
    }, 

    buttonCancel: { 
        marginTop: 15,
        backgroundColor: '#ccc',
    }
});